/*
 * Copyright (c) 2014 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.xbee;

import com.rapplogic.xbee.api.XBeeAddress64;



/**
 * Created by hansolo on 19.09.14.
 */
public enum XBees {
    SENSOR_0("0x00,0x13,0xa2,0x00,0x40,0xc0,0x50,0x66", "sensor0");

    public final String        ID;
    public final String        NAME;
    public final XBeeAddress64 ADDRESS_64;


    // ******************** Constructor ***************************************
    private XBees(final String ID, final String NAME) {
        this.ID   = ID;
        this.NAME = NAME;

        // Create XBeeAddress64 from ID
        String[] hexStringArray = this.ID.replaceAll("0x", "").split(",");
        if (hexStringArray.length != 8) { throw new IllegalArgumentException("XBee ID must contain 8 values."); }

        int[] intArray = new int[8];
        for (int i = 0 ; i < 8 ; i++) {
            intArray[i] = Integer.parseInt(hexStringArray[i], 16);
        }
        this.ADDRESS_64 = new XBeeAddress64(intArray);
    }
}
