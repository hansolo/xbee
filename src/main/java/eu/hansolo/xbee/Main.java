/*
 * Copyright (c) 2014 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.xbee;

import com.rapplogic.xbee.api.XBeeException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


/**
 * User: hansolo
 * Date: 16.10.13
 * Time: 16:15
 */
public class Main {
    private static DateTimeFormatter TF = DateTimeFormatter.ofPattern("HH:mm:ss");

    public Main() throws XBeeException {
        this("/dev/tty.usbserial-A601LPJH");
    }
    public Main(final String SERIAL_PORT) throws XBeeException {
        
        XBeeReceiver.INSTANCE.addXBee(XBees.SENSOR_0.ID, XBees.SENSOR_0.NAME);
        
        XBeeReceiver.INSTANCE.setOnXBeeEventReceived(xBeeEvent -> {
            System.out.println("XBee ID            : " + xBeeEvent.xbeeName);    
            System.out.println("Time               : " + TF.format(LocalDateTime.now()));            
            System.out.println("XBee AD3           : " + String.format(Locale.US, "%.5f", xBeeEvent.valueAD3));
            System.out.println("XBee AD4 on        : " + xBeeEvent.isAD4On);
            System.out.println("XBee supply voltage: " + xBeeEvent.supplyVoltage);
            System.out.println();            
        });
        XBeeReceiver.INSTANCE.setComPort(SERIAL_PORT);
        XBeeReceiver.INSTANCE.start();        
    }

    public static void main(String[] args) throws XBeeException {
        new Main();
    }
}
