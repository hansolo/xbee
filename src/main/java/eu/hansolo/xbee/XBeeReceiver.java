/*
 * Copyright (c) 2014 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.xbee;

import com.rapplogic.xbee.api.*;
import com.rapplogic.xbee.api.zigbee.ZNetRxIoSampleResponse;
import com.rapplogic.xbee.util.ByteUtils;
import eu.mihosoft.vrl.system.VSysUtil;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import org.apache.commons.io.FileUtils;


/**
 * User: hansolo Date: 16.10.13 Time: 16:16
 */
public enum XBeeReceiver {
    INSTANCE;

    private static final RemoteAtRequest D4_HIGH      = new RemoteAtRequest(XBees.SENSOR_0.ADDRESS_64, "D4", new int[]{5});
    private static final RemoteAtRequest D4_LOW       = new RemoteAtRequest(XBees.SENSOR_0.ADDRESS_64, "D4", new int[]{4});
    private static final RemoteAtRequest D3_ANALOG_IN = new RemoteAtRequest(XBees.SENSOR_0.ADDRESS_64, "D3", new int[]{2});

    private Map<String, String> xbees   = new HashMap<>();
    private String              comPort = "tty.usbserial-A601LPJH"; // for Pi: "/dev/ttyAMA0";
    private XBee                xbee    = new XBee();
    private PacketListener      packetListener;


    // ******************** Constructors **************************************
    private XBeeReceiver() {
        initNativeLib();
        initListeners();
    }


    // ******************** Initialization ************************************
    private void initNativeLib() {
        String libName = "librxtxSerial." + VSysUtil.getPlatformSpecificLibraryEnding();
        try {
            URL  inputUrl = getClass().getResource("/xbee/natives/" + VSysUtil.getPlatformSpecificPath() + "/" + libName);
            File dest     = new File("/tmp/xbee/" + libName);
            FileUtils.copyURLToFile(inputUrl, dest);
            VSysUtil.addNativeLibraryPath(dest.getParentFile().getAbsolutePath());
        } catch (Exception ex) {
            Logger.getLogger(XBeeReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initListeners() {
        packetListener = response -> {
            if (ApiId.ZNET_IO_SAMPLE_RESPONSE == response.getApiId()) {
                ZNetRxIoSampleResponse rx = (ZNetRxIoSampleResponse) response;

                fireXBeeEvent(new XBeeEvent(ByteUtils.toBase16(rx.getRemoteAddress64().getAddress()),
                        rx.getAnalog3(),
                        rx.isD4On(),
                        rx.getSupplyVoltage() * 3.6 / 1024.0 * 1.38856, // connected to 3.3 V powersource
                        XBeeEvent.XBEE_EVENT_RECEIVED));
                try {
                    xbee.sendAsynchronous(rx.isD4On() ? D4_LOW : D4_HIGH);
                } catch (XBeeException exception) {

                }
            }
        };
    }


    // ******************** Methods *******************************************
    public void setComPort(final String COM_PORT) {
        comPort = COM_PORT;
        System.setProperty("gnu.io.rxtx.SerialPorts", comPort);
    }

    public void start() throws XBeeException {
        if (null == comPort || comPort.isEmpty()) { return; }
        try {
            xbee.open(comPort, 9600);
            xbee.addPacketListener(packetListener);

            System.out.println(xbee.isConnected());

            AtCommandResponse response = (AtCommandResponse) xbee.sendSynchronous(D4_LOW, 5000);

            if (response.isOk()) { System.out.println(response); }

            while (true) {

            }
        } finally {
            if (xbee.isConnected()) {
                xbee.removePacketListener(packetListener);
                xbee.close();
            }
        }
    }

    public void stop() {
        if (xbee.isConnected()) {
            xbee.removePacketListener(packetListener);
            xbee.close();
        }
    }

    public void addXBee(String ID, String name) { xbees.put(ID, name); }


    // ******************** Event Handling ************************************
    public final ObjectProperty<EventHandler<XBeeEvent>> onXBeeEventReceivedProperty() { return onXBeeEventReceived; }
    public final void setOnXBeeEventReceived(EventHandler<XBeeEvent> handler) { onXBeeEventReceivedProperty().set(handler); }
    public final EventHandler<XBeeEvent> getOnXBeeEventReceived() { return onXBeeEventReceivedProperty().get(); }
    private final ObjectProperty<EventHandler<XBeeEvent>> onXBeeEventReceived = new ObjectPropertyBase<EventHandler<XBeeEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onXBeeEventReceived"; }
    };

    public void fireXBeeEvent(final XBeeEvent EVENT) {
        final EventHandler<XBeeEvent> HANDLER = getOnXBeeEventReceived();
        if (null == HANDLER) return;

        HANDLER.handle(EVENT);
    }


    // ******************** Inner Classes *************************************
    public static class XBeeEvent extends Event {
        public static final EventType<XBeeEvent> XBEE_EVENT_RECEIVED = new EventType(ANY, "XBEE_EVENT_RECEIVED");
        public String  xbeeId;
        public String  xbeeName;
        public double  valueAD3;
        public boolean isAD4On;
        public double  supplyVoltage;

        // ******************* Constructors ***************************************        
        public XBeeEvent(final String ID, final double VALUE_AD3, final boolean IS_AD4_ON, final double SUPPLY_VOLTAGE, final Object SOURCE) {
            super(SOURCE, null, XBEE_EVENT_RECEIVED);
            xbeeId        = ID;
            xbeeName      = INSTANCE.xbees.containsKey(ID) ? INSTANCE.xbees.get(ID) : "unknown";
            valueAD3      = VALUE_AD3;
            isAD4On       = IS_AD4_ON;
            supplyVoltage = SUPPLY_VOLTAGE;
        }
    }
}
