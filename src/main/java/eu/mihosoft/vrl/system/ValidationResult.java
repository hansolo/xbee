package eu.mihosoft.vrl.system;

/**
 * Validation result.
 * @see VParamUtil
 * @author Michael Hoffer <info@michaelhoffer.de>
 */
public class ValidationResult {
    private String message;
    private boolean valid;
    private Object parameter;
    
    /**
     * Result that represents a valid parameter.
     */
    public static final ValidationResult VALID =
            new ValidationResult(true, null, null);
    
    /**
     * Result that represents an invalid parameter.
     */
    public static final ValidationResult INVALID = 
            new ValidationResult(false, null, null);

    /**
     * Constructor.
     * @param valid defines whether this result represents a valid parameter
     * @param message result message (may be <code>null</code>)
     * @param parameter validated parameter (may be <code>null</code>)
     */
    public ValidationResult(boolean valid, Object parameter, String message) {
        this.message = message;
        this.valid = valid;
        this.parameter = parameter;
    }
    
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the validation state
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @return the parameter
     */
    public Object getParameter() {
        return parameter;
    }
}
