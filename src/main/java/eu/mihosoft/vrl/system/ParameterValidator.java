package eu.mihosoft.vrl.system;

/**
 * A parameter validator.
 * @see VParamUtil
 * @author Michael Hoffer <info@michaelhoffer.de>
 */
public interface ParameterValidator {
    /**
     * Validates the specified parameter.
     * @param p parameter to validate
     * @return validation result
     */
    public ValidationResult validate(Object p);
    /**
     * Validates the specified parameter.
     * @param p parameter to validate
     * @param validationArg validationArg validation argument 
     * (may be null, usage depends on the validator implementation)
     * @return validation result
     */
    public ValidationResult validate(Object p, Object validationArg);
}
