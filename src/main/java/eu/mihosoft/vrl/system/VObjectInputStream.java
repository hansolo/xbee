package eu.mihosoft.vrl.system;


import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * VRL uses this input stream to ensure that classes defined as abstract code
 * and some other serialized classes/objects can be loaded. There is usally no
 * need to use this input stream outside of VRL core classes.
 *
 * @author Michael Hoffer <info@michaelhoffer.de>
 */
public class VObjectInputStream extends ObjectInputStream {

    private ClassLoader classLoader;

    /**
     * Constructor.
     *
     * @param in the input stream to use
     * @param classLoader the class loader to use
     * @throws java.io.IOException
     */
    public VObjectInputStream(
            InputStream in, ClassLoader classLoader) throws IOException {
        super(in);
        this.classLoader = classLoader;
    }

    @Override
    protected Class<?> resolveClass(
            ObjectStreamClass desc) throws ClassNotFoundException {        
//        return Class.forName(desc.getName(), false, classLoader);


        return VClassLoaderUtil.forName(desc.getName(), classLoader);
    }
}
